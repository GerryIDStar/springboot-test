package com.self.demo.controller;
import com.self.demo.enums.GetRequestEnum;
import org.oorsprong.websamples.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListsInfoController {

    @RequestMapping("/listOfContinentsByCode")
    public ArrayOftContinent listOfContinentsByCode(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfContinentsByCode();
    }

    @RequestMapping("/listOfContinentsByName")
    public ArrayOftContinent listOfContinentsByName(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfContinentsByName();
    }

    @RequestMapping("/listOfCountryNamesByCode")
    public ArrayOftCountryCodeAndName listOfCountryNamesByCode(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfCountryNamesByCode();
    }

    @RequestMapping("/listOfCountryNamesByName")
    public ArrayOftCountryCodeAndName listOfCountryNamesByName(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfCountryNamesByName();
    }

    @RequestMapping("/listOfCountryNamesGroupedByContinent")
    public ArrayOftCountryCodeAndNameGroupedByContinent listOfCountryNamesGroupedByContinent(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfCountryNamesGroupedByContinent();
    }

    @RequestMapping("/listOfCurrenciesByCode")
    public ArrayOftCurrency listOfCurrenciesByCode(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfCurrenciesByCode();
    }

    @RequestMapping("/listOfCurrenciesByName")
    public ArrayOftCurrency listOfCurrenciesByName(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfCurrenciesByName();
    }

    @RequestMapping("/listOfLanguagesByCode")
    public ArrayOftLanguage listOfLanguagesByCode(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfLanguagesByCode();
    }

    @RequestMapping("/listOfLanguagesByName")
    public ArrayOftLanguage listOfLanguagesByName(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.listOfLanguagesByName();
    }

    @RequestMapping("/fullCountryInfoAllCountries")
    public ArrayOftCountryInfo fullCountryInfoAllCountries(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.fullCountryInfoAllCountries();
    }

    @RequestMapping("/countriesUsingCurrency")
    public ArrayOftCountryCodeAndName countriesUsingCurrency(String currencyCode){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.countriesUsingCurrency(currencyCode);
    }

}
