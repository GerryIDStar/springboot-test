package com.self.demo.controller;
import com.self.demo.enums.GetRequestEnum;
import org.oorsprong.websamples.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CountryInfoController {

    private String getInfo(GetRequestEnum reqEnum, String queryStr){
        String str;
        try {
            CountryInfoService countryInfoService = new CountryInfoService();
            CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();

            switch (reqEnum){
                case COUNTRYFLG:
                    str = countryInfoServiceSoapType.countryFlag(queryStr);
                    break;
                case CAPITALCITY:
                    str = countryInfoServiceSoapType.capitalCity(queryStr);
                    break;
                case COUNTRYNAME:
                    str = countryInfoServiceSoapType.countryName(queryStr);
                    break;
                case PHONECODE:
                    str = countryInfoServiceSoapType.countryIntPhoneCode(queryStr);
                    break;
                case COUNTRYCURRENCY:
                    TCurrency currency = countryInfoServiceSoapType.countryCurrency(queryStr);
                    str = "ISO: " + currency.getSISOCode() + "\nName: " + currency.getSName();
                    break;
                /*case FULLCOUNTRYINFO:
                    TCountryInfo countryInfo = countryInfoServiceSoapType.fullCountryInfo(queryStr);

                    StringBuilder languageString = new StringBuilder("Language(s): ");
                    List<TLanguage> languageList = countryInfo.getLanguages().getTLanguage();
                    for (TLanguage language:languageList) {
                        languageString.append(language.getSName()).append(",");
                    }

                    str = "Name: " + countryInfo.getSName() +
                            "\nISO Code: " + countryInfo.getSCurrencyISOCode() +
                            "\nCountry Flag: " + countryInfo.getSCountryFlag() +
                            "\nCapital City: " + countryInfo.getSCapitalCity() +
                            "\nCurrency ISO Code: " + countryInfo.getSCurrencyISOCode() +
                            "\nContinent Code: " + countryInfo.getSContinentCode() +
                            "\nPhone Code: " + countryInfo.getSPhoneCode() +
                            "\nLanguages: " + languageString.toString();
                    break;*/
                case CURRENCYNAME:
                    str = countryInfoServiceSoapType.currencyName(queryStr);
                    break;
                case COUNTRYISOCODE:
                    str = countryInfoServiceSoapType.countryISOCode(queryStr);
                    break;
                case LANGUAGENAME:
                    str = countryInfoServiceSoapType.languageName(queryStr);
                    break;
                case LANGUAGEISOCODE:
                    str = countryInfoServiceSoapType.languageISOCode(queryStr);
                    break;
                default:
                    str = "Invalid request";
                break;
            }
            System.out.println(str);
        }
        catch (Exception e){
            str = e.getMessage();
        }
        return str;
    }

    @RequestMapping("/capitalCity")
    public String getCapitalCity(String countryISOCode){
        return getInfo(GetRequestEnum.CAPITALCITY, countryISOCode);
    }

    @RequestMapping("/countryFlag")
    public String getCountryFlag(String countryISOCode){
        return getInfo(GetRequestEnum.COUNTRYFLG, countryISOCode);
    }

    @RequestMapping("/countryName")
    public String getCountryName(String countryISOCode){
        return getInfo(GetRequestEnum.COUNTRYNAME, countryISOCode);
    }

    @RequestMapping("/phoneCode")
    public String getPhoneCode(String countryISOCode){
        return getInfo(GetRequestEnum.PHONECODE, countryISOCode);
    }

    @RequestMapping("/countryCurrency")
    public String getCountryCurrency(String countryISOCode){
        return getInfo(GetRequestEnum.COUNTRYCURRENCY, countryISOCode);
    }

    @RequestMapping("/fullCountryInfo")
    public TCountryInfo getFullCountryInfo(String countryISOCode){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        return countryInfoServiceSoapType.fullCountryInfo(countryISOCode);

        //return getInfo(GetRequestEnum.FULLCOUNTRYINFO, countryISOCode);
    }

    @RequestMapping("/currencyName")
    public String getCurrencyName(String currencyISOCode){
        return getInfo(GetRequestEnum.CURRENCYNAME, currencyISOCode);
    }

    @RequestMapping("/countryISOCode")
    public String getCountryISOCode(String countryName){
        return getInfo(GetRequestEnum.COUNTRYISOCODE, countryName);
    }

    @RequestMapping("/languageName")
    public String getLanguageName(String languageISOCode){
        return getInfo(GetRequestEnum.LANGUAGENAME, languageISOCode);
    }

    @RequestMapping("/languageISOCode")
    public String getLanguageISOCode(String languageName){
        return getInfo(GetRequestEnum.LANGUAGEISOCODE, languageName);
    }

/*                case CURRENCYNAME:
    str = countryInfoServiceSoapType.currencyName(queryStr);
                    break;
                case COUNTRYISOCODE:
    str = countryInfoServiceSoapType.countryISOCode(queryStr);
                    break;
                case LANGUAGENAME:
    str = countryInfoServiceSoapType.languageName(queryStr);
                    break;
                case LANGUAGEISOCODE:
    str = countryInfoServiceSoapType.languageISOCode(queryStr);
                    break;*/



}
