package com.self.demo.controller;

import org.oorsprong.websamples.CountryInfoService;
import org.oorsprong.websamples.CountryInfoServiceSoapType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/")
    public String getSomething(){
        CountryInfoService countryInfoService = new CountryInfoService();
        CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
        String str = countryInfoServiceSoapType.capitalCity("RUS");
        System.out.println(str);
        return str;
        /*try {
            CountryInfoService countryInfoService = new CountryInfoService();
            CountryInfoServiceSoapType countryInfoServiceSoapType = countryInfoService.getCountryInfoServiceSoap();
            String str = countryInfoServiceSoapType.capitalCity("RUS");
            System.out.println(str);
            if(str.equals("Moscow"))
                return "OK";
            else return str;
        }
        catch (Exception e){
            return e.getMessage();
        }*/
    }
}
